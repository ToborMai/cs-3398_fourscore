import Game.*;

import java.util.*;
import org.junit.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;

public class GameEngineTest {

    @BeforeEach
    public void initEach() {
    }

    @Test
    public void GameEngineConstructorTest()
    {
        GameEngine gameEngine = new GameEngine();

        for (int i = 0; i < 7; ++i)
        {
            for (int j = 0; j < 6; ++j)
            {
                assertEquals(gameEngine.checkTile(i,j), 0);
            }
        }
    }

    @Test
    public void GameEngineConstructorTestWithInput()
    {
        Random rand = new Random();
        int x = rand.nextInt(100);
        int y = rand.nextInt(100);

        GameEngine gameEngine = new GameEngine(x, y);

        for (int i = 0; i < x; ++i)
        {
            for (int j = 0; j < y; ++j)
            {
                assertEquals(0,gameEngine.checkTile(i,j));
            }
        }
    }

    @Test
    public void displayTextBoardTest()
    {
        GameEngine gameEngine = new GameEngine();
        gameEngine.displayTextBoard();
        System.out.print("\n");
    }

    @Test
    public void placeCheckerTest()
    {
        GameEngine gameEngine = new GameEngine();

        Random rand = new Random();
        for (int i = 0; i < 200; ++i)
        {
            int x = rand.nextInt(7);
            int y = rand.nextInt(6);
            gameEngine.placeChecker(x,y, 1);
            if (gameEngine.validCheckerPlacement(x,y))
            {
                assertEquals(1,gameEngine.checkTile(x,y));
            }
        }
        gameEngine.displayTextBoard();
        System.out.print("\n");
    }

    @Test
    public void FourConnectedRandTest()
    {
        GameEngine gameEngine = new GameEngine();
        Random rand = new Random();
        for (int i = 0; i < 40; ++i)
        {
            int x = rand.nextInt(7);
            int y = rand.nextInt(6);
            gameEngine.placeChecker(x,y, 1);
            if(gameEngine.FourConnected(x,y,1))
            {
                System.out.println("Four connected!");
                gameEngine.displayTextBoard();
            }
        }
    }
}
