import javax.swing.*;

class MenuFactory
{
    public static JPanel createMenu(MenuType type)
    {
        Menu menu = null;

        switch (type)
        {
            case TitleScreen:
                menu = new TitleScreen();
                break;
            case MainMenu:
                menu = new MainMenu();
                break;
            case OptionsMenu:
                menu = new OptionsMenu();
                break;
            case GameMenu:
                menu = new GameMenu();
                break;
            //TODO insert default statement
            default:
                throw new IllegalArgumentException("No such Menu");
        }
        return menu;
    }

    public static JPanel createMenu(String type)
    {
        Menu menu = null;

        switch (type)
        {
            case "TitleScreen":
                menu = new TitleScreen();
                break;
            case "MainMenu":
                menu = new MainMenu();
                break;
            case "OptionsMenu":
                menu = new OptionsMenu();
                break;
            case "GameMenu":
                menu = new GameMenu();
                break;
            //TODO insert default statement
            default:
                throw new IllegalArgumentException("No such Menu");
        }
        return menu;
    }
}
