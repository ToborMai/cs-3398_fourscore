package Game;

public class GameEngine {

    private int[][] gameBoard;
    int xDimension;
    int yDimension;

    private char cellPosition;
    private int rowPosition;
    private int cellState;


    /**
     * Constructs an empty gameBoard, uses default dimensions of 7x6
     */
    public GameEngine()
    {
//        newBoard(7, 6);
        cellPosition = ' ';
        rowPosition = 0;
        cellState = 0;
    }

    /**
     * Constructs an empty gameBoard, using provided dimensions
     */
    public GameEngine(int x, int y)
    {
        newBoard(x, y);
    }

    /**
     * Initializes gameBoard to an empty array
     *
     * @param x the length of the x dimension of the board
     * @param y the length of the x dimension of the board
     */
    public void newBoard(int x, int y)
    {
        this.xDimension = x;
        this.yDimension = y;
        gameBoard = new int[x][y];
    }

    /**
     * placeChecker() is used to add checkers from the gameBoard. It will automatically check if
     * the coordinates are valid
     * @param x the x-coordinate at which to place a checker
     * @param y the x-coordinate at which to place a checker
     * @param checker the color of the checker to be placed (should be 1 for player 1, 2 for player 2)
     * @return if piece was placed successfully
     */
    public boolean placeChecker(int x, int y, int checker)
    {
        if (validCheckerPlacement(x, y))
        {
            gameBoard[x][y] = checker;
            return true;
        }
        return false;
    }

    /**
     * Returns value of element at coordinates
     * @param x the x-coordinate of the location to be checked
     * @param y the y-coordinate of the lcoation to be checked
     * @return the value of the element at [x][y]. 0 is empty, 1 is player 1, 2 is player 2, -1 is error
     */
    public int checkTile(int x, int y)
    {
        try {
            return gameBoard[x][y];
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
            return -1;
        }
    }

    /**
     * Checks gameBoard at the input coordinates. Returns true if it is a valid placement location for a checker
     * returns false if it is not a valid location for a checker.
     *
     * @param x the x-coordinate of the location to be checked
     * @param y the y-coordinate of the lcoation to be checked
     * @return is valid checker placement?
     */
    public boolean validCheckerPlacement(int x, int y)
    {
        try
        {
            if (gameBoard[x][y] != 0)               // invalid if place is taken
                return false;
            else if (x > this.xDimension || y > this.yDimension)      // invalid if placed out of dimensions
                return false;
            else if (y > 0 && gameBoard[x][y - 1] == 0)         // invalid if no piece below
                    return false;
            else
                return true;
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            //TODO Controller.getMenu.close()
            return false;
        }
    }

    /**
     * Checks to see if checker at x,y coordinate is part of a line of 4
     * @param x x-coordinate of checker
     * @param y y-coordinate of checker
     * @param color color of checker (0 is empty, 1 is player 1, 2 is player 2, -1 is error)
     * @return
     */
    public boolean FourConnected(int x, int y, int color)
    {
        int line = 0;
        // Horizontal
        for (int i = 0; i < xDimension; ++i)
        {
            if (gameBoard[i][y] == color)
                line++;
            else
                line = 0;
            if (line >= 4)
                return true;
        }
        line = 0;
        // Vertical
        for (int i = 0; i < yDimension; ++i)
        {
            if (gameBoard[x][i] == color)
                line++;
            else
                line = 0;
            if (line >= 4)
                return true;
        }
        return false;
    }

    /**
     * Displays current gameBoard as text to terminal
     */
    public void displayTextBoard()
    {
        for (int j = yDimension-1; j >= 0; --j)
        {
            for (int i = xDimension-1; i >= 0; --i)
            {
                System.out.print(gameBoard[i][j]);
            }
            System.out.print("\n");
        }
    }


    /**
     * Constructor that takes cell position and row position
     *
     * @param newCellPosition character to cell position
     * @param newRowPosition  integer to row position
     */
    public GameEngine(char newCellPosition, int newRowPosition)
    {
        cellPosition = newCellPosition;
        rowPosition = newRowPosition;
    }

    public void setCellPosition(char newCellPosition)
    {
        cellPosition = newCellPosition;
    }

    /**
     * Set the row position
     *
     * @param newRowPosition integer to row position
     */
    public void setRowPosition(int newRowPosition)
    {
        rowPosition = newRowPosition;
    }

    /**
     * Set cell and row position
     *
     * @param newCellPosition character to cell position
     * @param newRowPosition  integer to row position
     */
    public void setAllPosition(char newCellPosition, int newRowPosition)
    {
        cellPosition = newCellPosition;
        rowPosition = newRowPosition;
    }

    /**
     * Set the cell state
     *
     * @param newCellState integer to cell state
     */
    public void setCellState(int newCellState)
    {
        cellState = newCellState;
    }

    /**
     * Getter to cell state
     *
     * @return cell state
     */
    public int getCellState()
    {
        return cellState;
    }

    /**
     * Get Cell Position
     *
     * @return cell position
     */
    public char getCellPosition()
    {
        return cellPosition;
    }


    /**
     * Getter the row position
     *
     * @return row position
     */
    public int getRowPosition()
    {
        return rowPosition;
    }



}
