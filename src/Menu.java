import javax.swing.*;

public abstract class Menu extends JPanel
{
    private final MenuType type;

    public Menu(MenuType type)
    {
        this.type = type;
    }
    // all abstract methods

    public abstract JPanel getPanel();

    public abstract String requestOtherMenu(MenuType type);

    public abstract String requestOtherMenu(String type);

    @Override
    public String toString()
    {
        return "Type- " + type;
    }
}

