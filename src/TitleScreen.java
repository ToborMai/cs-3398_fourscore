import javax.swing.*;


public class TitleScreen extends Menu
{
    private JPanel titlePanel;
    private JPanel titleTitlePanel;
    private JPanel titleSubtitlePanel;
    private JPanel titleLogoPanel;
    private JPanel titleCreditsPanel;
    private JFrame frame;


    public TitleScreen()
    {
        super(MenuType.TitleScreen);
        getPanel();
    }

    @Override
    public JPanel getPanel()
    {
        add(titlePanel);
        return this;
    }

    @Override
    public String requestOtherMenu(MenuType type)
    {
        return null;
    }


    @Override
    public String requestOtherMenu(String newMenu)
    {
        return null;
    }
}
