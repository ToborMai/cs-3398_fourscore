import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static java.sql.DriverManager.println;

class MyFrame extends JFrame
{
    static MenuFactory factory;
    static JPanel panel;
    static int count = 0;
    private static String menu;
    private static volatile MyFrame instance = null;
    static String type = "TitleScreen";

    public static MyFrame getInstance()
    {
        if (instance == null)
        {
            synchronized (MyFrame.class)
            {
                if (instance == null)
                {
                    instance = new MyFrame(type);
                    instance.dispose();
                    instance.setLocationRelativeTo(null); //set to center of screen
                    instance.setBackground(Color.WHITE);
                    panel = MenuFactory.createMenu(type);
                    instance.getContentPane().add(panel);
                    instance.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    instance.setSize(1000, 1000);
                    instance.setVisible(true);
                    instance.pack();
                    instance.setTitle(menu);
                    instance.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                }
            }
        }
        return instance;
    }

    public MyFrame(String type)
    {
        // Frame Parameters
        setTitle(menu);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        // Window Listeners
        addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                System.exit(0);
            } //windowClosing
        }); //addWindowLister

        if (count < 1)
        {
            setTimer();
            count++;
        }

        System.out.print("inside constructor ");

    } //constructor MyFrame


    public void setTimer()

    {
        int delay = 2000; //milliseconds

        ActionListener taskPerformer = evt ->
        {
            this.getContentPane().remove(panel);
            this.getContentPane().repaint();
            panel = MenuFactory.createMenu("MainMenu");
            this.getContentPane().add(panel);
            this.setVisible(true);

            ((Timer) evt.getSource()).stop();
        };
        new Timer(delay, taskPerformer).start();
    }


    public void setMenu(String type)
    {
        menu = type;
        System.out.print("menu: " + menu);
    }

    public void getMenu()
    {
        this.getContentPane().remove(panel);
        this.getContentPane().repaint();
        panel = MenuFactory.createMenu(menu);
        this.getContentPane().add(panel);
        this.setVisible(true);
//        setTimer();
    }


} //class MyFrame
