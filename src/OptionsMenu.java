import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OptionsMenu extends Menu
{
    private JPanel optionsPanel;
    private JRadioButton easyRadioButton;
    private JRadioButton mediumRadioButton;
    private JRadioButton hardRadioButton;
    private JButton REDButton;
    private JComboBox comboBox1;
    private JPanel optionsLogo;
    private JSpinner spinner1;
    private JSpinner spinner2;
    private JButton confirmButton;
    private JButton resetToDefaultButton;
    private JButton returnToMainMenuButton;
    private JButton continueToGameButton;
    private JButton REDButton1;


    public OptionsMenu()
    {
        super(MenuType.OptionsMenu);
        createUIComponents();
        getPanel();
        returnToMainMenuButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                requestOtherMenu("MainMenu");
            }
        });
        continueToGameButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                requestOtherMenu("GameMenu");
            }
        });
    }

    private void createUIComponents()
    {
        // custom component creation code here
        // set range for time limit spinners
        SpinnerNumberModel m_numberSpinnerModel_1;
        SpinnerNumberModel m_numberSpinnerModel_2;
        Double current = new Double(0.00);
        Double min = new Double(0.00);
        Double max = new Double(60.00);
        Double step = new Double(1);
        m_numberSpinnerModel_1 = new SpinnerNumberModel(current, min, max, step);
        m_numberSpinnerModel_2 = new SpinnerNumberModel(current, min, max, step);

        spinner1 = new JSpinner(m_numberSpinnerModel_1);
        spinner2 = new JSpinner(m_numberSpinnerModel_2);

    }

    @Override
    public JPanel getPanel()
    {
        add(optionsPanel);
        return this;
    }

    @Override
    public String requestOtherMenu(MenuType type)
    {
        return null;
    }

    @Override
    public String requestOtherMenu(String type)
    {
        MyFrame app = MyFrame.getInstance();
        app.setMenu(type);
        app.getMenu();
        return type;
    }


}
