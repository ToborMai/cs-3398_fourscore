import javax.swing.*;
import java.awt.event.*;

//import static com.sun.tools.internal.xjc.reader.Ring.add;

public class MainMenu extends Menu
{
    private JPanel mainPanel;
    private JPanel panel;
    private JButton singlePlayerButton;
    private JButton a2PlayerButton;
    private JButton optionsButton;
    private JPanel mainTitlePanel;
    private JPanel mainSubtitlePanel;
    private JPanel mainPlayerPanel;
    private JPanel mainOptionsPanel;
    private JPanel mainLogoPanel;
    private JPanel mainCreditsPanel;
    private MyFrame frame;

    MainMenu()
    {
        super(MenuType.MainMenu);
        getPanel();


        singlePlayerButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                //set player to single
            }
        });
        optionsButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                requestOtherMenu("OptionsMenu");

            }
        });
    }

    @Override
    public JPanel getPanel()
    {
        add(mainPanel);
        return this;
    }

    @Override
    public String requestOtherMenu(MenuType type)
    {
        return null;
    }

    @Override
    public String requestOtherMenu(String type)
    {
        MyFrame app = MyFrame.getInstance();
        app.setMenu(type);
        app.getMenu();
        return type;
    }

    public void close()
    {
        this.setVisible(false);
    }

}
