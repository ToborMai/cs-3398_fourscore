package ButtonListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener
{

    JFrame parent;

    public ButtonListener(JFrame parent)
    {
        this.parent = parent;

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        JOptionPane.showMessageDialog(parent, "You pressed a button");
    }

}
