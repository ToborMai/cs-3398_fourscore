/*Switch between Menus*/

import ButtonListener.ButtonListener;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.tools.Tool;
import java.awt.*;
import java.awt.Menu;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.TimerTask;

public class Controller extends JFrame
{
    //    private static Controller frame;
    static MenuFactory factory;
    static Menu menu;
    static JFrame frame;
    static JPanel panel;
    Window window;
    static MyFrame iFrame;

    public Controller()
    {

    }

    public void loadMenu(String type)
    {
        panel = MenuFactory.createMenu(type);
        frame = new JFrame("Rabbit Army GUI");
        frame.setBackground(Color.WHITE);
        frame.getContentPane().add(panel);

        frame.add(MenuFactory.createMenu(type));

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(600, 600);
        frame.setVisible(true);
        frame.pack();
        frame.setTitle(type);
        frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
    }

    public void clearMenu()
    {
        frame.setVisible(false);
        frame.dispose();
        WindowEvent winClosingEvent = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
        ;
    }

    // timer for transition from Title Screen to Main Screen on initialization
    public static void setTimer()
    {
        int delay = 5000; //milliseconds

        ActionListener taskPerformer = evt ->
        {

//            clearMenu();loadMenu("MainMenu");

            ((Timer) evt.getSource()).stop();
        };
        new Timer(delay, taskPerformer).start();
    }


    public static void initialization() throws IOException
    {
//        MyFrame frame = new MyFrame("TitleScreen");
        MyFrame app = MyFrame.getInstance();
        app.setMenu("TitleScreen");
//        frame.setVisible(true);
        app.setTimer();
    }

    public static void main(String args[])
    {

        Controller controller = new Controller();
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    initialization();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }
}